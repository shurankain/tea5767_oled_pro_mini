#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>
#include <TEA5767Radio.h>

TEA5767Radio radio = TEA5767Radio();
float freq = 100.0;

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET 4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void printFreq(){
  display.clearDisplay();
  display.setTextSize(2);      // Normal 1:1 pixel scale
  display.setTextColor(WHITE); // Draw white text
  display.setCursor(15, 0);     // Start at top-left corner
  display.println(F("FM RADIO"));
  display.setCursor(35, 16);     // Start at top-left corner
  display.println(F("freq: "));
  if(freq >= 100){
    display.setCursor(25, 36);  
  }else{
    display.setCursor(35, 36);  
  }
  display.println(freq);
  
  display.setCursor(0, 36);
  display.println("<");
  display.setCursor(116, 36);
  display.println(">");
  display.display();
  delay(100);
}

void printFreqDown(){
  display.setTextColor(BLACK);
  display.setCursor(0, 36);
  display.print("<");
  display.display();
  delay(100);
  printFreq();
}

void printFreqUp(){
  display.setTextColor(BLACK);
  display.setCursor(116, 36);
  display.print(">");
  display.display();
  delay(100);
  printFreq();
}

void setFreq(){
  if(freq > 106.0){
    freq = 95.0;
  }
  if(freq < 95.0){
    freq = 106.0;
  }  
  radio.setFrequency(freq);
}

void setup()
{
  pinMode(10,INPUT_PULLUP);
  pinMode(11,INPUT_PULLUP);
  Serial.begin(9600);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C))
  { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ; // Don't proceed, loop forever
  }

  // Show initial display buffer contents on the screen --
  // the library initializes this with an Adafruit splash screen.
  display.display();
  delay(2000);

  setFreq();
  printFreq();

  Wire.begin();
  radio.setFrequency(freq); // pick your own frequency
}

void loop()
{
  Wire.begin(); 

  if(!digitalRead(11)){
    freq -= 0.1;
    setFreq();
    printFreqDown();
  }
  if(!digitalRead(10)){
    freq += 0.1;
    setFreq();
    printFreqUp();
  }
};
